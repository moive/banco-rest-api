<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index');
$routes->post('/auth/login', 'Auth::login');

/**
 * http://localhost:8080/api/
 */
$routes->group('api', ['namespace' => 'App\Controllers\API', 'filter' => 'authFilter'], function ($routes) {

	/**
	 * http://localhost:8080/api/clients ---> GET
	 */
	$routes->get('clients', 'Clients::index');
	$routes->post('clients/create', 'Clients::create');
	$routes->get('clients/edit/(:num)', 'Clients::edit/$1');
	$routes->put('clients/update/(:num)', 'Clients::update/$1');
	$routes->delete('clients/delete/(:num)', 'Clients::delete/$1');

	$routes->get('account', 'Account::index');
	$routes->post('account/create', 'Account::create');
	$routes->get('account/edit/(:num)', 'Account::edit/$1');
	$routes->put('account/update/(:num)', 'Account::update/$1');
	$routes->delete('account/delete/(:num)', 'Account::delete/$1');
	$routes->get('account', 'Account::index');

	$routes->get('transactiontype/', 'TransactionType::index');
	$routes->post('transactiontype/create', 'TransactionType::create');
	$routes->get('transactiontype/edit/(:num)', 'TransactionType::edit/$1');
	$routes->put('transactiontype/update/(:num)', 'TransactionType::update/$1');
	$routes->delete('transactiontype/delete/(:num)', 'TransactionType::delete/$1');

	$routes->get('transaction/', 'Transaction::index');
	$routes->post('transaction/create', 'Transaction::create');
	$routes->get('transaction/edit/(:num)', 'Transaction::edit/$1');
	$routes->put('transaction/update/(:num)', 'Transaction::update/$1');
	$routes->delete('transaction/delete/(:num)', 'Transaction::delete/$1');
	$routes->get('transaction/client/(:num)', 'Transaction::getTransactionByClient/$1');

	$routes->get('roles/', 'Roles::index');
	$routes->post('roles/create', 'Roles::create');
	$routes->get('roles/edit/(:num)', 'Roles::edit/$1');
	$routes->put('roles/update/(:num)', 'Roles::update/$1');
	$routes->delete('roles/delete/(:num)', 'Roles::delete/$1');

	$routes->get('users/', 'Users::index');
	$routes->post('users/create', 'Users::create');
	$routes->get('users/edit/(:num)', 'Users::edit/$1');
	$routes->put('users/update/(:num)', 'Users::update/$1');
	$routes->delete('users/delete/(:num)', 'Users::delete/$1');
});

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
