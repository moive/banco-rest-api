<?php

namespace App\Models\CustomRules;

use App\Models\AccountModel;
use App\Models\ClientModel;
use App\Models\RolModel;
use App\Models\TransactionTypeModel;

class MyCustomRules
{
    public function is_valid_client(int $id): bool
    {
        $model = new ClientModel();
        $client = $model->find($id);

        return $client == null ? false : true;
    }

    public function is_valid_currency(string $currency): bool
    {
        switch ($currency):
            case 'USD':
                return true;
                break;

            case 'EUR':
                return true;
                break;

            default:
                return false;
                break;
        endswitch;
    }

    public function is_valid_account(int $id): bool
    {
        $model = new AccountModel();
        $account = $model->find($id);

        return $account == null ? false : true;
    }

    public function is_valid_transaction_type(int $id): bool
    {
        $model = new TransactionTypeModel();
        $transactionType = $model->find($id);

        return $transactionType == null ? false : true;
    }

    public function is_valid_rol(int $id): bool
    {
        $model = new RolModel();
        $rol = $model->find($id);

        return $rol == null ? false : true;
    }
}
