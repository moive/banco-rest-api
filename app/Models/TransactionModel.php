<?php

namespace App\Models;

use CodeIgniter\Model;

class TransactionModel extends Model
{
    protected $table = 'transaccion';
    protected $primaryKey = 'id';

    protected $returnType = 'array';
    protected $allowedFields = ['cuenta_id', 'tipo_transaccion_id', 'monto'];

    protected $useTimestamps = true;
    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';

    protected $validationRules = [
        'monto' => 'required|numeric',
        'cuenta_id' => 'required|integer|is_valid_account',
        'tipo_transaccion_id' => 'required|integer|is_valid_transaction_type',
    ];

    protected $validationMessages = [
        'cuenta_id' => [
            'is_valid_account' => 'Dear user, you must enter a valid account'
        ],
        'tipo_transaccion_id' => [
            'is_valid_transaction_type' => 'Dear user, you must enter a valid transaction type'
        ],
    ];

    protected $skyValidation = false;

    public function TransactionByClient($clientId = null)
    {
        $builder = $this->db->table($this->table);
        $builder->select('cuenta.id AS AccountNumber, cliente.nombre, cliente.apellido');
        $builder->select('tipo_transaccion.descripcion AS Tipo, transaccion.monto, transaccion.created_at AS TransactionDate');
        $builder->join('cuenta', 'transaccion.cuenta_id = cuenta.id');
        $builder->join('tipo_transaccion', 'transaccion.tipo_transaccion_id = tipo_transaccion.id');
        $builder->join('cliente', 'cuenta.cliente_id = cliente.id');
        $builder->where('cliente.id', $clientId);

        $query = $builder->get();
        return $query->getResult();
    }
}
