<?php

namespace App\Controllers\API;

use App\Models\AccountModel;
use App\Models\ClientModel;
use App\Models\TransactionModel;
use CodeIgniter\RESTful\ResourceController;

class Transaction extends ResourceController
{
    public function __construct()
    {
        $this->model = $this->setModel(new TransactionModel());
        helper('access_rol');
    }

    public function index()
    {
        try {
            if (!validateAccess(array('admin', 'ATM'), $this->request->getServer('HTTP_AUTHORIZATION')))
                return $this->failServerError('The role does not have access to this resource');

            $transaction = $this->model->findAll();
            return $this->respond($transaction);
        } catch (\Exception $e) {
            return $this->failServerError('A server error has ocurred');
        }
    }

    public function create()
    {
        try {
            $transaction = $this->request->getJSON();
            if ($this->model->insert($transaction)) :
                $transaction->id = $this->model->insertID();
                $transaction->result = $this->updateAccountFund($transaction->tipo_transaccion_id, $transaction->monto, $transaction->cuenta_id);

                return $this->respondCreated($transaction);
            else :
                return $this->failValidationError($this->model->validation->listErrors());
            endif;
        } catch (\Exception $e) {
            return $this->failServerError('A server error has ocurred');
        }
    }

    public function edit($id = null)
    {
        try {
            if ($id == null)
                return $this->failValidationError($this->model->validation->listErrors());

            $transaction = $this->model->find($id);

            if ($transaction == null)
                return $this->failNotFound('No transaction type found with id: ' . $id);
            return $this->respond($transaction);
        } catch (\Exception $e) {
            return $this->failServerError('A server error has ocurred');
        }
    }

    public function update($id = null)
    {
        try {
            if ($id == null)
                return $this->failValidationError('No valid Id been passed');

            $verifiedTransaction = $this->model->find($id);

            if ($verifiedTransaction == null)
                return $this->failNotFound('No transaction type found with id: ' . $id);

            $transaction = $this->request->getJSON();

            if ($this->model->update($id, $transaction)) :
                $transaction->id = $id;
                return $this->respondUpdated($transaction);
            else :
                return $this->failValidationError($this->model->validation->listErrors());
            endif;
        } catch (\Exception $e) {
            return $this->failServerError('A server error has ocurred');
        }
    }

    public function delete($id = null)
    {
        try {
            if ($id == null)
                return $this->failValidationError('No valid Id been passed');

            $verifiedTransaction = $this->model->find($id);

            if ($verifiedTransaction == null)
                return $this->failNotFound('No transaction type found with id: ' . $id);

            if ($this->model->delete($id)) :
                return $this->respondDeleted($verifiedTransaction);
            else :
                return $this->failServerError('Record could not be deleted');
            endif;
        } catch (\Exception $e) {
            return $this->failServerError('A server error has ocurred');
        }
    }

    public function getTransactionByClient($id = null)
    {
        try {
            if (!validateAccess(array('client'), $this->request->getServer('HTTP_AUTHORIZATION')))
                return $this->failServerError('The role does not have access to this resource');

            $modelClient = new ClientModel();
            if ($id == null)
                return $this->failValidationError('No valid Id been passed');

            $client = $modelClient->find($id);
            if ($client == null)
                return $this->failNotFound('No customer found with id: ' . $id);
            $transaction = $this->model->TransactionByClient($id);
            return $this->respond($transaction);
        } catch (\Exception $e) {
            return $this->failServerError('A server error has ocurred');
        }
    }

    private function updateAccountFund($transactionTypeId, $amount, $accountId)
    {
        $accountModel = new AccountModel();
        $account = $accountModel->find($accountId);

        switch ($transactionTypeId) {
            case 1:
                $account['fondo'] += $amount;
                break;

            case 2:
                $account['fondo'] -= $amount;
                break;
        }

        if ($accountModel->update($accountId, $account)) :
            return array('SuccessfulTransaction' => true, 'NewFund' => $account['fondo']);
        else :
            return array('SuccessfulTransaction' => false, 'NewFund' => $account['fondo']);
        endif;
    }
}
