<?php

namespace App\Controllers\API;

use App\Models\TransactionTypeModel;
use CodeIgniter\RESTful\ResourceController;

class TransactionType extends ResourceController
{
    public function __construct()
    {
        $this->model = $this->setModel(new TransactionTypeModel());
    }

    public function index()
    {
        $transactionType = $this->model->findAll();
        return $this->respond($transactionType);
    }

    public function create()
    {
        try {
            $transactionType = $this->request->getJSON();
            if ($this->model->insert($transactionType)) :
                $transactionType->id = $this->model->insertID();
                return $this->respondCreated($transactionType);
            else :
                return $this->failValidationError($this->model->validation->listErrors());
            endif;
        } catch (\Exception $e) {
            return $this->failServerError('A server error has ocurred');
        }
    }

    public function edit($id = null)
    {
        try {
            if ($id == null)
                return $this->failValidationError($this->model->validation->listErrors());

            $transactionType = $this->model->find($id);

            if ($transactionType == null)
                return $this->failNotFound('No transaction type found with id: ' . $id);
            return $this->respond($transactionType);
        } catch (\Exception $e) {
            return $this->failServerError('A server error has ocurred');
        }
    }

    public function update($id = null)
    {
        try {
            if ($id == null)
                return $this->failValidationError('No valid Id been passed');

            $verifiedTransactionType = $this->model->find($id);

            if ($verifiedTransactionType == null)
                return $this->failNotFound('No transaction type found with id: ' . $id);

            $transactionType = $this->request->getJSON();

            if ($this->model->update($id, $transactionType)) :
                $transactionType->id = $id;
                return $this->respondUpdated($transactionType);
            else :
                return $this->failValidationError($this->model->validation->listErrors());
            endif;
        } catch (\Exception $e) {
            return $this->failServerError('A server error has ocurred');
        }
    }

    public function delete($id = null)
    {
        try {
            if ($id == null)
                return $this->failValidationError('No valid Id been passed');

            $verifiedTransactionType = $this->model->find($id);

            if ($verifiedTransactionType == null)
                return $this->failNotFound('No transaction type found with id: ' . $id);

            if ($this->model->delete($id)) :
                return $this->respondDeleted($verifiedTransactionType);
            else :
                return $this->failServerError('Record could not be deleted');
            endif;
        } catch (\Exception $e) {
            return $this->failServerError('A server error has ocurred');
        }
    }
}
