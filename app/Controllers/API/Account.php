<?php

namespace App\Controllers\API;

use App\Models\AccountModel;
use CodeIgniter\RESTful\ResourceController;

class Account extends ResourceController
{
    public function __construct()
    {
        $this->model = $this->setModel(new AccountModel());
    }

    public function index()
    {
        $accounts = $this->model->findAll();
        return $this->respond($accounts);
    }

    public function create()
    {
        try {
            $account = $this->request->getJSON();

            if ($this->model->insert($account)) :
                $account->id = $this->model->insertID();
                return $this->respondCreated($account);
            else :
                return $this->failValidationError($this->model->validation->listErrors());
            endif;
        } catch (\Exception $e) {
            return $this->failServerError('A server error has ocurred');
        }
    }

    public function edit($id = null)
    {
        try {
            if ($id == null)
                return $this->failValidationError('No valid Id been passed');

            $account = $this->model->find($id);

            if ($account == null)
                return $this->failNotFound('No account found with id: ' . $id);

            return $this->respond($account);
        } catch (\Exception $e) {
            return $this->failServerError('A server error has ocurred');
        }
    }

    public function update($id = null)
    {
        try {
            if ($id == null)
                return $this->failValidationError('No valid Id been passed');

            $verifiedAccount = $this->model->find($id);

            if ($verifiedAccount == null)
                return $this->failNotFound('No account found with id: ' . $id);

            $account = $this->request->getJSON();

            if ($this->model->update($id, $account)) :
                $account->id = $id;
                return $this->respondUpdated($account);
            else :
                return $this->failValidationError($this->model->validation->listErrors());
            endif;
        } catch (\Exception $e) {
            return $this->failServerError('A server error has ocurred');
        }
    }

    public function delete($id = null)
    {
        try {
            if ($id == null)
                return $this->failValidationError('No valid Id been passed');

            $verifiedAccount = $this->model->find($id);

            if ($verifiedAccount == null)
                return $this->failNotFound('No account found with id: ' . $id);

            if ($this->model->delete($id)) :
                return $this->respondDeleted($verifiedAccount);
            else :
                return $this->failServerError('Record could not be deleted');
            endif;
        } catch (\Exception $e) {
            return $this->failServerError('A server error has ocurred');
        }
    }
}
