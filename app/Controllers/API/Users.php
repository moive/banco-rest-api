<?php

namespace App\Controllers\API;

use App\Models\UserModel;
use CodeIgniter\RESTful\ResourceController;

class Users extends ResourceController
{
    public function __construct()
    {
        $this->model = $this->setModel(new UserModel());
        helper('secure_password');
    }

    public function index()
    {
        $users = $this->model->findAll();
        return $this->respond($users);
    }

    public function create()
    {
        try {
            $user = $this->request->getJSON();
            $user->password = hashPassword($user->password);

            if ($this->model->insert($user)) :
                $user->id = $this->model->insertID();
                return $this->respondCreated($user);
            else :
                return $this->failValidationError($this->model->validation->listErrors());
            endif;
        } catch (\Exception $e) {
            return $this->failServerError('A server error has ocurred');
        }
    }

    public function edit($id = null)
    {
        try {
            if ($id == null)
                return $this->failValidationError('No valid Id been passed');

            $user = $this->model->find($id);
            if ($user == null)
                return $this->failNotFound('No customer found with id: ' . $id);

            return $this->respond($user);
        } catch (\Exception $e) {
            return $this->failServerError('A server error has ocurred');
        }
    }


    public function update($id = null)
    {
        try {
            if ($id == null)
                return $this->failValidationError('No valid Id been passed');

            $verifiedUser = $this->model->find($id);
            if ($verifiedUser == null)
                return $this->failNotFound('No rol found with id: ' . $id);

            $user = $this->request->getJSON();
            $user->password = hashPassword($user->password);

            if ($this->model->update($id, $user)) :
                $user->id = $id;
                return $this->respondUpdated($user);
            else :
                return $this->failValidationError($this->model->validation->listErrors());
            endif;
        } catch (\Exception $e) {
            return $this->failServerError('A server error has ocurred');
        }
    }

    public function delete($id = null)
    {
        try {
            if ($id == null)
                return $this->failValidationError('No valid Id been passed');

            $verifiedUser = $this->model->find($id);
            if ($verifiedUser == null)
                return $this->failNotFound('No customer found with id: ' . $id);

            if ($this->model->delete($id)) :
                return $this->respondDeleted($verifiedUser);
            else :
                return $this->failServerError('Record could not be deleted');
            endif;
        } catch (\Exception $e) {
            return $this->failServerError('A server error has ocurred');
        }
    }
}
