<?php

namespace App\Controllers\API;

use App\Models\ClientModel;
use CodeIgniter\RESTful\ResourceController;

class Clients extends ResourceController
{
    public function __construct()
    {
        $this->model = $this->setModel(new ClientModel());
        helper('access_rol');
    }

    public function index()
    {
        try {
            if (!validateAccess(array('admin'), $this->request->getServer('HTTP_AUTHORIZATION')))
                return $this->failServerError('The role does not have access to this resource');

            $clients = $this->model->orderBy('id', 'desc')->findAll();
            return $this->respond($clients);
        } catch (\Exception $e) {
            return $this->failServerError('A server error has ocurred');
        }
    }

    public function create()
    {
        try {

            $client = $this->request->getJSON();

            if ($this->model->insert($client)) :
                return $this->respondCreated($client);
            else :
                return $this->failValidationError($this->model->validation->listErrors());
            endif;
        } catch (\Exception $e) {
            return $this->failServerError('A server error has ocurred');
        }
    }

    public function edit($id = null)
    {
        try {
            if ($id == null)
                return $this->failValidationError('No valid Id been passed');

            $client = $this->model->find($id);
            if ($client == null)
                return $this->failNotFound('No customer found with id: ' . $id);
            return $this->respond($client);
        } catch (\Exception $e) {
            return $this->failServerError('A server error has ocurred');
        }
    }


    public function update($id = null)
    {
        try {
            if ($id == null)
                return $this->failValidationError('No valid Id been passed');

            $verifiedClient = $this->model->find($id);
            if ($verifiedClient == null)
                return $this->failNotFound('No customer found with id: ' . $id);

            $client = $this->request->getJSON();

            if ($this->model->update($id, $client)) :
                $client->id = $id;
                return $this->respondUpdated($client);
            else :
                return $this->failValidationError($this->model->validation->listErrors());
            endif;
        } catch (\Exception $e) {
            return $this->failServerError('A server error has ocurred');
        }
    }

    public function delete($id = null)
    {
        try {
            if ($id == null)
                return $this->failValidationError('No valid Id been passed');

            $verifiedClient = $this->model->find($id);
            if ($verifiedClient == null)
                return $this->failNotFound('No customer found with id: ' . $id);

            if ($this->model->delete($id)) :
                return $this->respondDeleted($verifiedClient);
            else :
                return $this->failServerError('Record could not be deleted');
            endif;
        } catch (\Exception $e) {
            return $this->failServerError('A server error has ocurred');
        }
    }
}
