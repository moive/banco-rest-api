<?php

namespace App\Controllers\API;

use App\Models\RolModel;
use CodeIgniter\RESTful\ResourceController;

class Roles extends ResourceController
{
    public function __construct()
    {
        $this->model = $this->setModel(new RolModel());
    }

    public function index()
    {
        $roles = $this->model->findAll();
        return $this->respond($roles);
    }

    public function create()
    {
        try {
            $rol = $this->request->getJSON();
            if ($this->model->insert($rol)) :
                $rol->id = $this->model->insertID();
                return $this->respondCreated($rol);
            else :
                return $this->failValidationError($this->model->validation->listErrors());
            endif;
        } catch (\Exception $e) {
            return $this->failServerError('A server error has ocurred');
        }
    }

    public function edit($id = null)
    {
        try {
            if ($id == null)
                return $this->failValidationError('No valid Id been passed');

            $rol = $this->model->find($id);
            if ($rol == null)
                return $this->failNotFound('No customer found with id: ' . $id);

            return $this->respond($rol);
        } catch (\Exception $e) {
            return $this->failServerError('A server error has ocurred');
        }
    }


    public function update($id = null)
    {
        try {
            if ($id == null)
                return $this->failValidationError('No valid Id been passed');

            $verifiedRol = $this->model->find($id);
            if ($verifiedRol == null)
                return $this->failNotFound('No rol found with id: ' . $id);

            $rol = $this->request->getJSON();

            if ($this->model->update($id, $rol)) :
                $rol->id = $id;
                return $this->respondUpdated($rol);
            else :
                return $this->failValidationError($this->model->validation->listErrors());
            endif;
        } catch (\Exception $e) {
            return $this->failServerError('A server error has ocurred');
        }
    }

    public function delete($id = null)
    {
        try {
            if ($id == null)
                return $this->failValidationError('No valid Id been passed');

            $verifiedRol = $this->model->find($id);
            if ($verifiedRol == null)
                return $this->failNotFound('No customer found with id: ' . $id);

            if ($this->model->delete($id)) :
                return $this->respondDeleted($verifiedRol);
            else :
                return $this->failServerError('Record could not be deleted');
            endif;
        } catch (\Exception $e) {
            return $this->failServerError('A server error has ocurred');
        }
    }
}
