<?php

namespace App\Controllers;

use App\Models\UserModel;
use CodeIgniter\API\ResponseTrait;
use Config\Services;
use Firebase\JWT\JWT;
// https://github.com/firebase/php-jwt
//https://jwt.io/

class Auth extends BaseController
{
    use ResponseTrait;

    public function __construct()
    {
        helper('secure_password');
    }

    public function login()
    {
        try {

            $username = $this->request->getPost('username');
            $password = $this->request->getPost('password');

            $userModel = new UserModel();

            $validateUser = $userModel->where('username', $username)->first();

            if ($validateUser == null)
                return $this->failNotFound('User not found');

            if (verifyPassword($password, $validateUser["password"])) :
                $jwt = $this->generateJWT($validateUser);
                return $this->respond(['Token' => $jwt], 201);
            else :
                return $this->failValidationError('Password invalid');
            endif;
        } catch (\Exception $e) {
            return $this->failServerError('A server error has ocurred');
        }
    }

    protected function generateJWT($user)
    {
        $key = Services::getSecretKey();
        $time = time();
        $payload = [
            'aud' => base_url(),
            'iat' => $time,
            'exp' => $time + 120,
            'data' => [
                'name' => $user['nombre'],
                'username' => $user['username'],
                'rol' => $user['rol_id'],
            ]
        ];

        $jwt = JWT::encode($payload, $key);
        return $jwt;
    }
}
